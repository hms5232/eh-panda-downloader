#!/usr/bin/env python3
# coding=utf-8

"""
Repo: https://gitlab.com/hms5232/eh-panda-downloader
"""


import os
import re

import requests
from bs4 import BeautifulSoup


folder_path = ''


def main():
	global folder_path
	
	add_header = {'User-Agent': 'Mozilla/5.0'}
	
	target_url = input('Please input URL: ')
	#TODO: 驗證網址
	print("進行請求中...", end='\r')
	r = requests.get(target_url, headers = add_header)
	if r.status_code != 200:
		print('Request failed!')
		return
	print("進行請求中... Done")
	
	print("解析回應中...", end='\r')
	soup = BeautifulSoup(r.content, 'html.parser')
	print("解析回應中... Done")
	
	'''
	建立資料夾
	
	熊貓相簿集的名稱有 gn 和 gj 兩種
	推測 j = Japan，因為如果有 gj 的 id 的標題多數都是日文
	總之以 gn 為主，因為不是所有的主題都有 gj 標題，就不費心去找出全部 h1 了
	'''
	folder_path = './{}/'.format(replace_win_reserved_characters(soup.h1.string))
	if not os.path.exists(folder_path):
		os.makedirs(folder_path)
	
	a_tags = soup.find_all('a')
	for tag in a_tags:
		# 尋找進入點
		if re.search(r"e\-hentai\.org\/s\/", tag.get('href')):
			first_img = tag.get('href')
			break
	print("Downloading...")
	download_img(first_img)


def download_img(img_page):
	add_header = {'User-Agent': 'Mozilla/5.0'}
	r = requests.get(img_page, headers = add_header)
	if r.status_code != 200:
		print('Request failed!')
		return
	soup = BeautifulSoup(r.content, 'html.parser')
	
	img_url = soup.find(id='img').get('src')
	div_tags = soup.find_all('div')
	for tag in div_tags:
		if tag.get('id') != "i1" and re.search(r" :: [0-9]", str(tag.string)):
			'''
			圖片檔名 :: 長寬 :: 檔案大小
			'''
			print(tag.string)
			img_title = tag.string.split(' :: ')[0]
			break
	
	s = requests.get(img_url, headers = add_header)  # 取自圖片頁面網址
	with open(folder_path+img_title, 'wb') as image:
		image.write(s.content)
		image.flush()  # flushes the internal buffer
	
	# 判斷中止或進行下一張圖的抓取
	if soup.find(id='next').get('href') == img_page:
		print("下載完成")
	else:
		return download_img(soup.find(id='next').get('href'))


def replace_win_reserved_characters(name):
	""" 使用底線替換 Windows 的保留字元 """

	'''
	Windows 檔案和路徑的保留字元
	https://docs.microsoft.com/zh-tw/windows/win32/fileio/naming-a-file

	< (小於，less than)
	> (大於，greater than)
	: (冒號，colon)
	" (雙引號，double quote)
	/ (正斜線，forward slash)
	\ (反斜線，backslash)
	| (分隔號形圖或管道（不知道是啥翻譯，我都稱豎線），vertical bar or pipe)
	? (問號，question mark)
	* (星號，asterisk)
	'''
	REVERSVED_CHAR_REG = r"[\<\>\:\"\/\\\|\?\*]"

	return re.sub(REVERSVED_CHAR_REG, '❄', name)


if __name__ == '__main__':
	main()
