# EH panda downloader

從熊貓網學習(◔౪◔)

## 需求
1. Python 3.6 或以上
2. 必須要額外安裝以下套件（可直接用 pip 安裝或使用 pipenv）
    1. requests
    2. Beautiful Soup
3. （´◔​∀◔`)夠大的空間及權限來裝圖(❍ᴥ❍ʋ)

## 使用方式
移動到專案目錄下後在終端機執行 `python3 shinshi.py` 後輸入網址就會開始下載，但請注意以下幾點：

1. 請輸入相簿第一頁的網址，否則會造成無法抓取前幾頁的圖
2. 相簿網址格式一律為 `https://e-hentai.org/g/{相簿ID}/{相簿token}/`
3. 有些 Windows 特殊保留字元會被取代成 `❄`

## 部署
* 直接安裝額外需求套件
    ```
    pip3 install requests
    pip3 install beautifulsoup4
    ```
* pipenv
    ```
    pipenv install
    ```

## 聲明
本人撰寫程式僅用於學術研究及學習，請勿用於非法用途。使用者一切行為所致皆由使用者自行承擔，與作者無任何關聯，亦不負任何責任。

## LICENSE
See [LICENSE file](https://gitlab.com/hms5232/eh-panda-downloader/-/blob/master/LICENSE)